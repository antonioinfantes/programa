package Programa;

import java.util.Date;
import java.util.Scanner;

/**
 * Clase pasarela de pago la cual realiza los pagos
 * y retorna el resultado.
 *
 * @author  Antonio Infantes
 * @since   22/02/2022
 *
 * @see @see <a href = "https://aulavirtual.elcampico.org/pluginfile.php/31045/mod_resource/content/1/Pra%CC%81ctica%20de%20programacio%CC%81n%202%C2%BA%20Evaluacio%CC%81n%202.pdf" />
 * Practica programación 2 Evaluación </a>
 */

public class PasarelaDePago {

    //Atributos
    private double importe;
    Date codigoPago = new Date();

    //Constructor
    public PasarelaDePago(){}
    public PasarelaDePago(double importe){
        this.importe = importe;
    }

    //Metodos
    //public Date getTime(){}

    /**
     * Metodo para efectuar el pago en efectivo
     *
     * @param aEntregar
     */
    public void efectivo(double aEntregar) {

        //Variables
        int billete50 = 0;
        int billete20 = 0;
        int billete10 = 0;
        int billete5 = 0;
        int euro = 0;
        double resultadoResto = 0;

        resultadoResto = aEntregar; //resto sin formatear

        System.out.println();
        System.out.println("SU CAMBIO");
        System.out.println("****************");

        //Algoritmo de pago efectivo
        //validacion 50billete
        for (int contador = 1; aEntregar >= 50; contador++) {
            aEntregar = aEntregar - 50;
            billete50 = contador;
        }
        System.out.println("50€    " + billete50 + " billete (s)");

        //validacion 20billete
        for (int contador = 1; aEntregar >= 20; contador++) {
            aEntregar = aEntregar - 20;
            billete20 = contador;
        }
        System.out.println("20€    " + billete20 + " billete (s)");

        //validacion 10billete
        for (int contador = 1; aEntregar >= 10; contador++) {
            aEntregar = aEntregar - 10;
            billete10 = contador;
        }
        System.out.println("10€    " + billete10 + " billete (s)");

        //validacion 5billete
        for (int contador = 1; aEntregar >= 5; contador++) {
            aEntregar = aEntregar - 5;
            billete5 = contador;
        }
        System.out.println("5€     " + billete5 + " billete (s)");

        //validacion 1euro
        for (int contador = 1; aEntregar >= 1; contador++) {
            aEntregar = aEntregar - 1;
            euro = contador;
        }

        System.out.println("1€     " + euro + " euro    (s)");
        System.out.println("cent   " + String.format("%.2f", aEntregar) + " cent (s)");

        System.out.println("Total devolución: " + String.format("%.2f", resultadoResto));
        System.out.println("**********************");
        System.out.println("GRACIAS POR SU VISITA");

    }

    /**
     * Metodo para efectuar el pago en tarjeta de credito
     *
     * @param numeroTarjeta
     * @return
     */
    public Boolean tarjeta(String numeroTarjeta){

        //validarEntero parse
        int resultadoCifra = 0;

        //Replace cadena
        numeroTarjeta = numeroTarjeta.replace("/", "");
        numeroTarjeta = numeroTarjeta.replace("-", "");
        numeroTarjeta = numeroTarjeta.replace(":", "");
        numeroTarjeta = numeroTarjeta.replace(" ", "");

        //Validar Numeros Letras Simbolos
        for (int i = 0; i < numeroTarjeta.length(); i++){
            if (numeroTarjeta.charAt(i) >= '0' && numeroTarjeta.charAt(i) <= '9') {
            } else {
                System.out.println("Valor incorrecto... Tienen que ser numeros");
                return true;
            }
        }

        //Validar digitos
        if (numeroTarjeta.length() == 16){
        } else {
            System.out.println("Valor incorrecto... (16 digitos)");
            return true;
        }

        //Mostrar Credit Card Number sin otros
        System.out.println("Credit Card Number: " + numeroTarjeta);

        //Validar Formato Tarjeta
        if (numeroTarjeta.charAt(0) == '3'){
            System.out.println("Formato American Express");
        } else if (numeroTarjeta.charAt(0) == '4'){
            System.out.println("Formato Visa");
        } else if (numeroTarjeta.charAt(0) == '5'){
            System.out.println("Formato MasterCard");
        } else{
            System.out.println("Valor incorrecto... Formato 3/4/5");
            return true;
        }

        for (int i = 0; i < numeroTarjeta.length(); i+=2){

            //Conversion de tipos
            char charTarjeta = numeroTarjeta.charAt(i);
            String stringTarjeta = Character.toString(charTarjeta);
            int runTarjeta = Integer.parseInt(stringTarjeta);

            //Validar y calculo
            if (runTarjeta*2 > 9){
                resultadoCifra = (runTarjeta - 9) + resultadoCifra;
            } else{
                resultadoCifra = runTarjeta + resultadoCifra;
            }
        }

        //Ultima validacion con resultado completo
        if (resultadoCifra%10 == 0 && resultadoCifra < 150){
            System.out.println("Numero de tarjeta " + numeroTarjeta + " es valido.");
        } else{
            System.out.println("Numero de tarjeta " + numeroTarjeta + " no es valido.");
            return true;
        }
        return false;
    }

    /**
     * Metodo para efectuar el pago en cuenta bancaria
     *
     * @param numeroCuenta
     * @return
     */
    public Boolean cuenta(String numeroCuenta){
        //Replace cadena
        numeroCuenta = numeroCuenta.replace("/", "");
        numeroCuenta = numeroCuenta.replace("-", "");
        numeroCuenta = numeroCuenta.replace(":", "");
        numeroCuenta = numeroCuenta.replace(" ", "");


        //Validar Numeros Letras Simbolos
        for (int i = 0; i < numeroCuenta.length(); i++){
            if (numeroCuenta.charAt(i) >= '0' && numeroCuenta.charAt(i) <= '9') {
            } else {
                System.out.println("Valor incorrecto... Tienen que ser numeros");
                return true;
            }
        }

        //Validar digitos
        if (numeroCuenta.length() == 20){
        } else {
            System.out.println("Valor incorrecto... (20 digitos)");
            return true;
        }

        //Pasar variable para resultado
        String cuentaCadena = numeroCuenta;
        System.out.println("Núm. CCC: " + numeroCuenta);


        //Quitando y coger digitos de control
        //Declarar variables
        int posicion = 8;
        String digitoControlCadena;
        digitoControlCadena = numeroCuenta.substring(8,10);
        numeroCuenta = numeroCuenta.substring(0, posicion) + numeroCuenta.substring(posicion + 2);

        //Agregar digitos
        String digitosCero = "00";
        numeroCuenta = digitosCero + numeroCuenta;

        //Coger dos cadenas para realizar los factores
        String cadenaCuentaPrimera = numeroCuenta.substring(0,10);
        String cadenaCuentaSegunda = numeroCuenta.substring(10,20);

        //Coger pesos
        String pesosFactores = "12485&9736";

        //Digitos de control
        int primerDigito = 0;
        int segundoDigito = 0;

        //Variables conversion de tipo
        char charCuenta;
        String stringCuenta;
        int runCuenta;
        char charFactor;
        String stringFactor;
        int runFactor;

        //Calculo digitos dos cadenas
        for (int i = 0; i < cadenaCuentaPrimera.length() && i < pesosFactores.length()
                && i < cadenaCuentaSegunda.length(); i++){

            //Conversion de tipos cadenaCuenta
            charCuenta = cadenaCuentaPrimera.charAt(i);
            stringCuenta = Character.toString(charCuenta);
            runCuenta = Integer.parseInt(stringCuenta);

            //Conversion de tipos factores
            charFactor = pesosFactores.charAt(i);
            if (charFactor == '&'){
                runFactor = 10;
            } else {
                stringFactor = Character.toString(charFactor);
                runFactor = Integer.parseInt(stringFactor);
            }
            runCuenta = runFactor * runCuenta;
            primerDigito = runCuenta + primerDigito; //agregando los valores

            charCuenta = cadenaCuentaSegunda.charAt(i);
            stringCuenta = Character.toString(charCuenta);
            runCuenta = Integer.parseInt(stringCuenta);
            runCuenta = runFactor * runCuenta;
            segundoDigito = runCuenta + segundoDigito;
            runCuenta = 0;
            runFactor = 0;
        }

        //Sacar resto + resta
        primerDigito = 11 - (primerDigito % 11);
        segundoDigito = 11 - (segundoDigito % 11);

        //Validacion 10/11 == 1,0
        if (primerDigito == 11){
            primerDigito = 0;
        } else if (primerDigito == 10){
            primerDigito = 1;
        } else if (segundoDigito == 11){
            segundoDigito = 0;
        } else if (segundoDigito == 10){
            segundoDigito = 1;
        }

        // Conversiones pasar digitos de control a int
        char charPrimerDigitoControl = digitoControlCadena.charAt(0);
        String stringPrimerDigitoControl = Character.toString(charPrimerDigitoControl);
        int primerDigitoControl = Integer.parseInt(stringPrimerDigitoControl);
        char charSegundoDigitoControl = digitoControlCadena.charAt(1);
        String stringSegundoDigitoControl = Character.toString(charSegundoDigitoControl);
        int segundoDigitoControl = Integer.parseInt(stringSegundoDigitoControl);

        //Comparacion cadenas
        if (primerDigitoControl == primerDigito && segundoDigitoControl == segundoDigito){
            System.out.println("La cuenta " + cuentaCadena + " es correcta. \nDC: " + primerDigito + "" + segundoDigito +
                    " -> " + digitoControlCadena);
            return false;
        } else {
            System.out.println("La cuenta " + cuentaCadena + " NO es correcta. \nDC: " + primerDigito + "" + segundoDigito +
                    " -> " + digitoControlCadena);
            return true;
        }
    }

    /**
     * Validar cantidad Importe  (Por ser double)
     *
     * @param cantidadImporte
     * @return boolean
     */
    public boolean validarCantidadImporte(String cantidadImporte){

        cantidadImporte = cantidadImporte.replaceFirst("[.]", "");

        for (int i = 0; i < cantidadImporte.length(); i++){
            if (cantidadImporte.charAt(i) >= '0' && cantidadImporte.charAt(i) <= '9') {
            } else {
                System.out.println("Valor incorrecto... Tienen que ser numeros");
                return true;
            }
        }
        return false;
    }

    /**
     * Validar distintas entradas del programa
     *
     * @param cantidaNumero
     * @return boolean
     */
    public boolean validarEspacioDistintoElemento(String cantidaNumero){
        if (cantidaNumero.isEmpty()){
            System.out.println("No se pueden poner enters, o espacios...");
            return true;
        }
        if (cantidaNumero.charAt(0) == '.' || cantidaNumero.charAt(0) == ','){
            System.out.println("No se pueden poner puntos o comas...");
            return true;
        }
        return false;
    }

    //Getter Setter
    public double getImporte() {
        return importe;
    }
    public Date getCodigoPago() {
        return codigoPago;
    }
    public void setImporte(double importe) {
        this.importe = importe;
    }
    public void setCodigoPago(Date codigoPago) {
        this.codigoPago = codigoPago;
    }
}

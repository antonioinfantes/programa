package Programa;

/**
 * Clase producto necesaria para poder toda la informacion
 * necesaria, y trabajar con ella.
 *
 * @author  Antonio Infantes
 * @since   22/02/2022
 *
 * @see @see <a href = "https://aulavirtual.elcampico.org/pluginfile.php/31045/mod_resource/content/1/Pra%CC%81ctica%20de%20programacio%CC%81n%202%C2%BA%20Evaluacio%CC%81n%202.pdf" />
 * Practica programación 2 Evaluación </a>
 */

public class Producto {

    //Atributos
    private String nombre;
    private double medida;
    /*
    Para limitar la cantidad de decimales, usaremos:
    --System.out.println(String.format("%.2f", precio)); String.format
    --System.out.printf("Valor: %.2f", precio ); numberformat
     */
    private double precio;

    //Constructor
    public Producto(String nombre, double medida, double precio){
        this.nombre = nombre.toUpperCase();
        this.medida = medida;
        this.precio = precio;
    }

    //Getter Setter
    public String getNombre() {
        return nombre;
    }
    public double getMedida() {
        return medida;
    }
    public double getPrecio() {
        return precio;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setMedida(double medida) {
        this.medida = medida;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
}

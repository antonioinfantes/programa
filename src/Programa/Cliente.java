package Programa;

import java.util.ArrayList;
import java.util.Date;

/**
 * Clase cliente necesaria para la informacion e instarciarlo
 * en las distintas clases.
 *
 * @author  Antonio Infantes
 * @since   22/02/2022
 *
 * @see @see <a href = "https://aulavirtual.elcampico.org/pluginfile.php/31045/mod_resource/content/1/Pra%CC%81ctica%20de%20programacio%CC%81n%202%C2%BA%20Evaluacio%CC%81n%202.pdf" />
 * Practica programación 2 Evaluación </a>
 */

public class Cliente {

    //Atributos
    private String nombre;
    private String apellido_1;
    private String apellido_2;
    Date fechaDeAlta = new Date();
    private String telefono;
    private String direccion;
    private ArrayList<Cita> historial = new ArrayList();

    //Constructor
    public Cliente(){}
    public Cliente(String nombre, String apellido_1, String apellido_2, String telefono, String direccion){
        this.nombre = nombre.toLowerCase();
        this.apellido_1 = apellido_1.toUpperCase();
        this.apellido_2 = apellido_2.toUpperCase();
        this.telefono = telefono;
        this.direccion = direccion;
    }

    //Metodos

    /**
     * Validacion Telefono
     *
     * @param telefono
     * @return boolean
     */
    public boolean validacionTelefono(String telefono){
        telefono = telefono.replace(" ", "");
        telefono = telefono.replace("-", "");

        //Recorrer la variable y que sea igual a 9 digitos
        if (telefono.length() == 9){
        } else {
            System.out.println("Valor incorrecto... (9 digitos)");
            return true;
        }

        /*
        Dos opciones para el primer digito:
        Si es 6 o 7 -> es de un movil
        Si es 8 o 9 -> es de un telefono fijo
         */
        if (telefono.charAt(0) == '6' || telefono.charAt(0) == '7'){
            System.out.println("Es un dispositivo movil");
        } else if (telefono.charAt(0) == '8' || telefono.charAt(0) == '9'){
            System.out.println("Es un telefono fijo");
        } else {
            System.out.println("Primer digito incorrecto..."+"\nDispositivo Movil 6 o 7"
                    +"\nTelefono Fijo 8 o 9");
            return true;
        }
        return false;
    }

    //TODO Validar String

    //TODO Validacion FechaDeAlta

    //Setter
    public void setNombre(String nombre) {
        this.nombre = nombre.toLowerCase();
    }
    public void setApellido_1(String apellido_1) {
        this.apellido_1 = apellido_1;
    }
    public void setApellido_2(String apellido_2) {
        this.apellido_2 = apellido_2;
    }
    public void setFechaDeAlta(Date fechaDeAlta) {
        this.fechaDeAlta = fechaDeAlta;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public void setHistorial(ArrayList historial) {
        this.historial = historial;
    }

    //Getter
    public String getNombre(){
        return nombre.toLowerCase();
    }
    public String getApellido_1() {
        return apellido_1;
    }
    public String getApellido_2() {
        return apellido_2;
    }
    public Date getFechaDeAlta() {
        return fechaDeAlta;
    }
    public String getTelefono() {
        return telefono;
    }
    public String getDireccion() {
        return direccion;
    }
    public ArrayList getHistorial() {
        return historial;
    }

}

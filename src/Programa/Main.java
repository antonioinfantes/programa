package Programa;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Clase desde la que lanzamos el menu necesario para
 * el programa.
 *
 * @author  Antonio Infantes
 * @since   22/02/2022
 *
 * @see @see <a href = "https://aulavirtual.elcampico.org/pluginfile.php/31045/mod_resource/content/1/Pra%CC%81ctica%20de%20programacio%CC%81n%202%C2%BA%20Evaluacio%CC%81n%202.pdf" />
 * Practica programación 2 Evaluación </a>
 */

public class Main {



    /**
     * Comprobaciones de distintas variables para opciones, textos...
     *
     * @param variableValidar
     * @param tipo
     * @return
     */
    public static boolean comprobacionString(String variableValidar, char tipo){

        //Validar Opcion Tipo Pago Char
        if (tipo == 'c'){
            if (variableValidar.isEmpty()){
                System.out.println("-- Caracter vacio --");
                return true;
            }
            for (int i = 0; i < variableValidar.length(); i++){
                if (variableValidar.charAt(i) >= '0' && variableValidar.charAt(i) <= '9') {
                    System.out.println("--Solo CARACTERES permitidos--");
                    return true;
                }
            }
            if(variableValidar.length() >= 2){
                System.out.println("-- Limite de caractares alcanzado, solo un caracter --");
                return true;
            }
        }

        //Validar Opcion Menu Principal
        if (tipo == 'm'){
            if (variableValidar.isEmpty()){
                System.out.println("-- Opcion vacia --");
                return true;
            }
            for (int i = 0; i < variableValidar.length(); i++){
                if (variableValidar.charAt(i) >= '0' && variableValidar.charAt(i) <= '9') {
                } else {
                    System.out.println("-- Solo Numeros --");
                    return true;
                }
            }
            if(variableValidar.length() >= 2){
                System.out.println("--Limite de caractares alcanzado--");
                return true;
            }
        }

        //Validar Opcion Menu Producto/Servicio
        if (tipo == 'o'){
            for (int i = 0; i < variableValidar.length(); i++){
                if (variableValidar.charAt(i) >= '0' && variableValidar.charAt(i) <= '9') {
                } else {
                    System.out.println("-- Solo Numeros --");
                    return true;
                }
            }
            if(variableValidar.length() >= 2){
                System.out.println("--Limite de caractares alcanzado en nombre o apellidos--");
                return true;
            }
            if (variableValidar.isEmpty()){
                System.out.println("-- Opcion vacia --");
                return true;
            }
        }

        //Validar telefono
        if (tipo == 't'){
            variableValidar = variableValidar.replace(" ", "");
            variableValidar = variableValidar.replace("-", "");

            if (variableValidar.isEmpty()){
                System.out.println("-- No se puede ingresar texto vacio --");
                return true;
            }

            if (variableValidar.length() == 9){
            } else {
                System.out.println("Valor incorrecto... (9 digitos)");
                return true;
            }

            for (int i = 0; i < variableValidar.length(); i++){
                if (variableValidar.charAt(i) >= '0' && variableValidar.charAt(i) <= '9'
                    || variableValidar.charAt(i) == '+') {
                } else {
                    return true;
                }
            }
        }

        //Validar Nombre, apellidos
        if (tipo == 'a'){
            if (variableValidar.isEmpty()){
                System.out.println("-- No se puede ingresar texto vacio --");
                return true;
            }
            for (int i = 0; i < variableValidar.length(); i++){
                if (variableValidar.charAt(i) >= '0' && variableValidar.charAt(i) <= '9') {
                    System.out.println("--Solo CARACTERES permitidos--");
                    return true;
                }
            }
            if(variableValidar.length() >= 25){
                System.out.println("--Limite de caractares alcanzado en nombre o apellidos--");
                return true;
            }
        }

        //Validar direcccion
        if(tipo == 'd'){

            if (variableValidar.isEmpty()){
                System.out.println("-- No se puede ingresar texto vacio --");
                return true;
            }
            for (int i = 0; i < variableValidar.length(); i++){
                if (variableValidar.charAt(i) >= '0' && variableValidar.charAt(i) <= '9') {
                    System.out.println("--Solo CARACTERES permitidos--");
                    return true;
                }
            }
            if(variableValidar.length() >= 100){
                System.out.println("--Limite de caractares alcanzado en direccion--");
                return true;
            }
        }

        return false;
    }

    /**
     * Validacion para saber si el cliente ya ha sido añadido
     *
     * @param telefono
     * @param misClientes
     * @return boolean
     */
    public static boolean validacionClienteTelefono (String telefono, ArrayList<Cliente> misClientes){
        for (int i=0;i<misClientes.size(); i++){
            if (telefono.equals(misClientes.get(i).getTelefono())){
                //sout con cliente encontrado
                return true;
            }
        }
        return false;
    }

    /**
     * Listar Productos por defecto
     *
     * @param listadoProductos
     */
    public static void mostrarProductos(ArrayList<Producto> listadoProductos){

        //Variables
        int opcionProducto=1;

        System.out.println("OPCION          PRODUCTO       PRECIO");
        System.out.println("--------      -----------      ------");
        for (int i=0;i<listadoProductos.size();i++){
            System.out.println(opcionProducto++ + ".      " + listadoProductos.get(i).getNombre() + "      "
                    + listadoProductos.get(i).getPrecio());
        }
    }

    /**
     * Metodo para seleccionar Productos
     *
     * @param listadoProductos
     * @param misProductos
     * @return misProductos
     */
    public static ArrayList<Producto> seleccionarProductos(ArrayList<Producto> listadoProductos,
                                                           ArrayList<Producto> misProductos){
        //Variables
        Scanner sc = new Scanner(System.in);
        String opcionPedirProducto;
        int opcionValidaProducto;
        boolean opcionBucle = true;

        //Listado de productos por defecto
        mostrarProductos(listadoProductos);

        do {
            //Creas el bucle con el listado
            System.out.println("CANTIDAD      PRODUCTO      PRECIO");
            System.out.println("--------      -----------      ------");
            for (int i=0;i<misProductos.size();i++){
                System.out.println("1        " + misProductos.get(i).getNombre() + "     "
                        + misProductos.get(i).getPrecio());
            }
            System.out.println("Añadir servicios: " +
                    "\n 0. Salir");
            do {
                opcionPedirProducto = sc.nextLine();
                //Llamar a metodo para validar la opcion
            } while (comprobacionString(opcionPedirProducto, 'o'));//o-> Validar Servicio/Producto

            //Parseo opcion String valido a opcion Int
            opcionValidaProducto = Integer.parseInt(opcionPedirProducto);

            //Validar opcion mayor a tamaño array
            if (opcionValidaProducto > listadoProductos.size()){
                System.out.println("No se ha encontrado el servicio");
                seleccionarProductos(listadoProductos, misProductos);
            }

            //Si la opcion es cero llama al Menu Principal
            if (opcionValidaProducto == 0){
                opcionBucle = false;
                //menuPrincipal(opcionPedirProducto);
            } else {
                //Agregar producto a los productos del Cliente
                misProductos.add(listadoProductos.get(opcionValidaProducto-1));
            }
        } while (opcionBucle);

        return misProductos;
    }

    /**
     * Mostrar Servicios por defecto
     *
     * @param listadoServicios
     */
    public static void mostrarServicios(ArrayList<Servicios> listadoServicios){
        int opcionServicio=1;
        System.out.println("OPCION      SERVICIO      PRECIO");
        System.out.println("--------      -----------      ------");
        for (int i=0;i<listadoServicios.size();i++){
            System.out.println(opcionServicio++ + ".      " + listadoServicios.get(i).getNombre() + "      "
                    + listadoServicios.get(i).getPrecio());
        }
    }

    /**
     * Mostrar para seleccionar Servicios
     *
     * @param listadoServicios
     * @param misServicios
     * @return misServicios
     */
    public static ArrayList<Servicios> seleccionarServicios(ArrayList<Servicios> listadoServicios,
                                                            ArrayList<Servicios> misServicios){

        //Variables
        Scanner sc = new Scanner(System.in);
        String opcionPedirServicio;
        int opcionValidaServicio;
        boolean opcionBucle = true;

        //Mostrar servicios por defecto llamando a metodo mostrarServicios con servicios añadidos en el main
        mostrarServicios(listadoServicios);

        //Mis Servicios Añadidos con un for
        do {
            //Creas el bucle con el listado
            System.out.println("CANTIDAD      SERVICIO      PRECIO");
            System.out.println("--------      -----------      ------");
            for (int i=0;i<misServicios.size();i++){
                System.out.println("1        " + misServicios.get(i).getNombre() + "     "
                        + misServicios.get(i).getPrecio());
            }
            System.out.println("Añadir servicios: " +
                    "\n 0. Salir");
            do {
                opcionPedirServicio = sc.nextLine();
                //Llamar a metodo para validar la opcion
            } while (comprobacionString(opcionPedirServicio, 'o')); //o-> Servicio/Producto

            //Parseo opcion String valido a opcion Int
            opcionValidaServicio = Integer.parseInt(opcionPedirServicio);

            //Validar opcion mayor a tamaño array
            if (opcionValidaServicio > listadoServicios.size()){
                System.out.println("No se ha encontrado el servicio");
                seleccionarServicios(listadoServicios, misServicios);
            }

            //TODO Validar que el servicio no sea el mismo
            //if contains

            //Si la opcion es cero llama al Menu Principal
            if (opcionValidaServicio == 0){
                opcionBucle = false;
                //menuPrincipal(opcionPedirServicio);
            } else {
                //Agregar producto a los servicios del Cliente
                misServicios.add(listadoServicios.get(opcionValidaServicio-1));
            }
        } while (opcionBucle);

        return misServicios;
    }

    /**
     * Menu principal de bienvenida con validaciones.
     * Solo muestra la bienvenida una vez cuando entramos al programa,
     * despues cuando se cambia de opcion solo salen las opciones a poner
     *
     * @param opcionAValidar
     * @return opcionValidada
     */
    public static int menuPrincipal(String opcionAValidar){
        int opcionValidada;
        Scanner sc = new Scanner(System.in);
        System.out.println();
        System.out.println("-------------------------------------");
        System.out.println("- Bienvenido A La Peluqueria LaPaca -");
        System.out.println("-------------------------------------");
        System.out.println();
        do {
            System.out.println("1. Gestión Cliente"); //Insertar, eliminar, seleccionar cliente
            System.out.println("2. Gestión Productos - En Mantenimiento"); //Insertar, eliminar, seleccionar productos
            System.out.println("3. Gestión Servicios - En Mantenimiento"); //Insertar, eliminar, seleccionar productos
            System.out.println("4. Alta de Cita"); //Alta de cita con cliente añadido
            System.out.println("0. Salir");
            System.out.print("Que opcion desea elegir: ");
            opcionAValidar = sc.nextLine();
        } while (comprobacionString(opcionAValidar, 'm')); //Llama tipo m -> Menu Principal
        opcionValidada = Integer.parseInt(opcionAValidar);
        return opcionValidada;
    }

    public static void main(String[] args) {

        //Atributos
        Cliente miCliente;
        Cita miCita;
        String nombre;
        String apellido_1;
        String apellido_2;
        String telefono;
        String direccion;
        String opcionMenuPrincipalValidar="";
        String tipoPagoAValidar;
        char tipoPago;
        double totalPago = 0.0;
        boolean menuFinalizado = true;
        ArrayList<Cita> misCitas = new ArrayList<Cita>();
        ArrayList<Cliente> listadoClientes = new ArrayList<Cliente>();
        ArrayList<Servicios> listadoServicios = new ArrayList<Servicios>();
        ArrayList<Servicios> miServicio = new ArrayList<Servicios>();
        ArrayList<Producto> listadoProductos = new ArrayList<Producto>();
        ArrayList<Producto> miProducto = new ArrayList<Producto>();
        Scanner sc = new Scanner(System.in);

        //Pone el cliente a null para siguientes validaciones
        miCliente = null;

        //Listado Servicios por defecto
        listadoServicios.add(new Servicios("CORTE PELO", "CORTE DE PELO", 10));
        listadoServicios.add(new Servicios("LAVADO PELO", "LAVADO DE PELO", 2.70));
        listadoServicios.add(new Servicios("TINTAR PELO", "TINTAR EL PELO", 20.3));
        listadoServicios.add(new Servicios("DEGRADADO PELO", "DEGRADADO DE PELO", 5));
        listadoServicios.add(new Servicios("RAYA PELO", "RAYA EN PELO", 3.50));
        listadoServicios.add(new Servicios("DIBUJO PELO", "DIBUJO EN PELO", 5));
        listadoServicios.add(new Servicios("TRENZAS", "TRENZAS EN EL PELO", 10));

        //Listado Productos por defecto
        listadoProductos.add(new Producto("CHAMPU DE HIERBAS", 1000, 4.83));
        listadoProductos.add(new Producto("CHAMPU HIDRATANTE", 250, 7.25));
        listadoProductos.add(new Producto("ACONDICIONADOR CABELLO SECO", 200, 9.88));
        listadoProductos.add(new Producto("ACONDICIONADOR LIGERO", 350, 4.54));
        listadoProductos.add(new Producto("MASCARILLA ALOE VERA", 700, 8.95));
        listadoProductos.add(new Producto("GEL EFECTO MOJADO", 150, 2.35));
        listadoProductos.add(new Producto("ESPUMA HIDRATANTE", 210, 2.45));

        //Listado Clientes por defecto
        listadoClientes.add(new Cliente("Antonio", "Infantes", "Marin",
                "600600600", "Calle España Nº17"));
        listadoClientes.add(new Cliente("Sofia", "Infantes", "Marin",
                "611611611", "Calle España Nº17"));
        listadoClientes.add(new Cliente("Pepe", "Ramirez", "Guzman",
                "622622622", "Calle Granada Nº18"));

        //Llamada a menu principal con validacion
        do {
            switch (menuPrincipal(opcionMenuPrincipalValidar)) {
                case 1:
                    //Crear cliente y validaciones en metodo Comprobacion Telefono y String
                    do {
                        System.out.println();
                        System.out.println("-----------------");
                        System.out.println("- GESTIÓN CLIENTE -");
                        System.out.println("-----------------");
                        System.out.println("Dime tu nombre: ");
                        nombre = sc.nextLine();
                        System.out.println("Dime tu primer apellido: ");
                        apellido_1 = sc.nextLine();
                        System.out.println("Dime tu segundo apellido: ");
                        apellido_2 = sc.nextLine();
                        System.out.println("Dime tu telefono: ");
                        telefono = sc.nextLine();
                        System.out.println("Dime tu direccion: ");
                        direccion = sc.nextLine();
                    } while (comprobacionString(nombre, 'n') || comprobacionString(apellido_1, 'n')
                            || comprobacionString(apellido_2, 'n') || comprobacionString(telefono, 't') ||
                            comprobacionString(direccion, 'd') || validacionClienteTelefono(telefono, listadoClientes));

                    //Confirmacion al añadir el cliente
                    System.out.println();
                    System.out.println("-- El cliente ha sido añadido a la base de datos --");
                    System.out.println();

                    //Inicializar cliente
                    miCliente = new Cliente(nombre, apellido_1, apellido_2, telefono, direccion);
                    break;
                case 2:
                    //TODO Lanzar metodo para gestionar productos
                    System.out.println();
                    System.out.println("---------------------------------------");
                    System.out.println("- Gestión Productos - En Mantenimiento-");
                    System.out.println("---------------------------------------");
                    seleccionarProductos(listadoProductos, miProducto);
                    break;
                case 3:
                    //TODO Lanzar metodo para gestionar servicios
                    System.out.println();
                    System.out.println("---------------------------------------");
                    System.out.println("- Gestión Servicios - En Mantenimiento-");
                    System.out.println("---------------------------------------");
                    seleccionarServicios(listadoServicios, miServicio);
                    break;
                case 4:
                    //Validacion para si un cliente existe
                    if (miCliente!=null){
                        System.out.println();
                        System.out.println("Cita agregada para cliente: "+miCliente.getNombre());

                        //Inicializar el cliente
                        miCita=new Cita(miCliente);

                        //Mostrar y agregar el listado de servicios
                        System.out.println();
                        System.out.println("---------------------");
                        System.out.println("- Agregar Servicios -");
                        System.out.println("---------------------");
                        seleccionarServicios(listadoServicios, miServicio);

                        //Coger total pago de mis servicios
                        for (int i=0;i<miServicio.size();i++){
                            totalPago = totalPago + miServicio.get(i).getPrecio();
                        }

                        //Mostrar y agregar el listado de productos
                        System.out.println();
                        System.out.println("---------------------");
                        System.out.println("- Agregar Productos -");
                        System.out.println("---------------------");
                        miCita.setProductos(seleccionarProductos(listadoProductos, miProducto));

                        //Coger total pago de mis productos
                        for (int i=0;i<miProducto.size();i++){
                            totalPago = totalPago + miProducto.get(i).getPrecio();
                        }

                        //Set pago para realizar el pago
                        miCita.setMiPago(new PasarelaDePago(totalPago));

                        //Menu de pago
                        System.out.println("----------------------------");
                        System.out.println("-Bienvenido al Menu de Pago-");
                        System.out.println("----------------------------");
                        System.out.println();
                        do {
                            System.out.println("Cantidad a pagar: " + totalPago);
                            System.out.println("Para seleccionar el tipo de pago: ");
                            System.out.println("C -> Cuenta Bancaria");
                            System.out.println("T -> Tarjeta de Credito");
                            System.out.println("E -> Efectivo");
                            System.out.println();
                            System.out.print("Como desea pagar: ");
                            tipoPagoAValidar=sc.nextLine(); //incluso next()
                        } while (comprobacionString(tipoPagoAValidar, 'c')); //caracter a validar

                        //Parseo a char
                        tipoPago = tipoPagoAValidar.charAt(0);

                        //Selecciona metodo
                        if (tipoPago == 'C'){
                            miCita.pagar('C');
                        } else if (tipoPago == 'T'){
                            miCita.pagar('T');
                        } else if (tipoPago == 'E'){
                            miCita.pagar('E');
                        } else {
                            System.out.println();
                            System.out.println("La opcion " + tipoPago + " no pertenece a ningun tipo de pago.");
                        }
                        System.out.println("Gracias por su atención " + miCliente.getNombre());
                        miCliente = null;
                    } else{
                        //Si no se ha agregado un cliente
                        System.out.println();
                        System.out.println("Se necesita cliente para agregar una cita." +
                                "\nPara crearlo elija la primera opcion."+
                                "\nGracias por su atención.");
                    }
                    break;
                case 0:
                    //Salir del programa
                    menuFinalizado=false;
                    System.out.println("Gracias por utilizar nuestro software.");
                    break;
                default:
                    System.out.println("No hay accion para esta opcion.");
            }
        } while (menuFinalizado);
    }
}

package Programa;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 * Clase cita para poder agregar citas y la cual nos retorna
 * el menu de bienvenida para poder realizar el pago.
 *
 * @author  Antonio Infantes
 * @since   22/02/2022
 *
 * @see @see <a href = "https://aulavirtual.elcampico.org/pluginfile.php/31045/mod_resource/content/1/Pra%CC%81ctica%20de%20programacio%CC%81n%202%C2%BA%20Evaluacio%CC%81n%202.pdf" />
 * Practica programación 2 Evaluación </a>
 */

public class Cita {

    //Inicializar atributos
    private double importeTotal = 100;
    private Cliente miCliente;
    private PasarelaDePago miPago;
    private Date fechaHora;
    Scanner sc = new Scanner(System.in);

    //TODO Realizarlos en constructor
    private ArrayList<Servicios> servicios = new ArrayList<Servicios>();
    private ArrayList<Producto> productos = new ArrayList<>();

    //Construstores, construyen arraylist
    public Cita(double importeTotal) {
        this.importeTotal = importeTotal;
    }
    public Cita(double importeTotal, Cliente miCliente){
        this.importeTotal = importeTotal;
        this.miCliente = miCliente;
    }
    public Cita(double importeTotal, Cliente miCliente, PasarelaDePago miPago, Date fechaHora){
        this.importeTotal = importeTotal;
        this.miCliente = miCliente;
        this.miPago = miPago;
        this.fechaHora = fechaHora;
    }

    public Cita(Cliente miCliente){
        this.miCliente = miCliente;
    }

    //Setter Getter
    public void setImporteTotal(double importeTotal) {
        this.importeTotal = importeTotal;
    }
    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }
    public void setMiCliente(Cliente miCliente) {
        this.miCliente = miCliente;
    }
    public void setMiPago(PasarelaDePago miPago) {
        this.miPago = miPago;
    }
    public void setServicios(ArrayList<Servicios> servicios) {
        this.servicios = servicios;
    }
    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }
    public double getImporteTotal() {
        return importeTotal;
    }
    public Date getFechaHora() {
        return fechaHora;
    }
    public Cliente getMiCliente() {
        return miCliente;
    }
    public PasarelaDePago getMiPago() {
        return miPago;
    }
    public ArrayList<Servicios> getServicios() {
        return servicios;
    }
    public ArrayList<Producto> getProductos() {
        return productos;
    }

    //Metodos

    /**
     * Metodo para una vez selecionado pedir la cantidad a entregar, numero de cuenta,
     * tarjeta de credito.
     *
     * @param tipoPago
     */
    public void pagar(char tipoPago){

        //Coger importe total
        miPago.getImporte();

        //Pago en Efectivo EFECTIVO
        if (tipoPago == 'E'){
            String cadenaEntregar;
            double aEntregar;
            System.out.println();
            System.out.println("Bienvenido al pago en efectivo.");
            //Validacion Primer Importe a Cantidad Entregar
            do {
                do {
                    System.out.print("\nIntroduce la cantidad a entregar (ej: 150.67): "); //nueva entrega
                    cadenaEntregar = sc.next(); // TODO Recojer con next Line
                } while (miPago.validarCantidadImporte(cadenaEntregar)
                        || miPago.validarEspacioDistintoElemento(cadenaEntregar));
                aEntregar = Double.parseDouble(cadenaEntregar);
            }while (aEntregar < miPago.getImporte()); //TODO comparar con total precio de productos o servicios
            aEntregar = aEntregar - miPago.getImporte(); //calculo resto
            miPago.efectivo(aEntregar);
        }

        //Pago de cuenta bancaria CUENTA BANCARIA
        if (tipoPago == 'C'){
            String numCuenta;
            System.out.println();
            System.out.println("Bienvenido al pago con cuenta bancaria.");

            numCuenta = sc.nextLine();
            do {
                //Pedir
                System.out.print("\nIntroduce el número de cuenta corriente (20 números): ");
                numCuenta = sc.nextLine();
                //TODO Poner validacion
            } while (miPago.cuenta(numCuenta));

            //salir
            System.out.println("Se le ha cobrado " + miPago.getImporte() + " euros");
            System.out.println("GRACIAS POR SU VISITA.");
        }

        //Pago en tarjeta de credito TARJETA DE CREDITO
        if (tipoPago == 'T'){
            String numTarjeta;
            System.out.println();
            System.out.println("Bienvenido al pago con tarjeta de credito.");

            //Pedir y realizar
            numTarjeta = sc.nextLine(); //Se pone para no repetir valor
            do {
                System.out.print("\nIntroduce el número de tu tarjeta de credito (16 digitos): ");
                numTarjeta = sc.nextLine();
                //TODO Hacer validacion
            } while (miPago.tarjeta(numTarjeta));

            //si completado
            System.out.println("Se le ha cobrado " + miPago.getImporte() + " euros");
            System.out.println("GRACIAS POR SU VISITA.");
        }
    }

    public void agregarServicio(Servicios servicios){
        this.servicios.add(servicios);
    }
    public void agregarProducto(Producto productos){
        this.productos.add(productos);
    }
    public void eliminarProducto(int posicion){}

    /**
     * Metodo toString para retornar datos
     *
     * @return datos
     */
    @Override
    public String toString(){
        String datos = "CANT.     SERVICIO     PRECIO UND.     TOTAL\n"
                + "=====     ========     ===========     =====\n";
        return datos;
    }
}

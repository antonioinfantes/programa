package Programa;

/**
 * Clase servicios necesaria para poder toda la informacion
 * necesaria, y trabajar con ella.
 *
 * @author  Antonio Infantes
 * @since   22/02/2022
 *
 * @see @see <a href = "https://aulavirtual.elcampico.org/pluginfile.php/31045/mod_resource/content/1/Pra%CC%81ctica%20de%20programacio%CC%81n%202%C2%BA%20Evaluacio%CC%81n%202.pdf" />
 * Practica programación 2 Evaluación </a>
 */

public class Servicios {

    //Atributos
    private String nombre;
    private String descripcion;
    /*
    Para limitar la cantidad de decimales, usaremos:
    --System.out.println(String.format("%.2f", precio)); String.format
    --System.out.printf("Valor: %.2f", precio ); numberformat
     */
    private double precio;

    //Constructor
    public Servicios(String nombre, String descripcion, double precio){
        this.nombre = nombre.toUpperCase();
        this.descripcion = descripcion;
        this.precio = precio;
    }

    //Getter Setter
    public String getNombre() {
        return nombre;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public double getPrecio() {
        return precio;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
